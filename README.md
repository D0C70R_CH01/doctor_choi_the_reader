# 최박사, 읽는 자

[![D0C70R_CH01](./d0c70r_ch01.gif "M4D3 I3Y D0C70R_CH01")](https://07001lab.tistory.com/)

[![TWITTER](./d0c70r_ch01/twitter.jpg  "TWITTER")](https://twitter.com/D0C70R_CH01) [![INSTAGRAM](./d0c70r_ch01/insta.jpg  "INSTAGRAM")](https://www.instagram.com/d0c70r_ch01/) [![GITLAB](./d0c70r_ch01/gitlab.jpg  "GITLAB")](https://gitlab.com/D0C70R_CH01) [![COFFEE](./d0c70r_ch01/coffee.jpg  "COFFEE")](https://www.buymeacoffee.com/d0c70rch01)

---

[![초대링크](./icon.png "초대링크")](https://discord.com/oauth2/authorize?client_id=1001200593328676895&permissions=36719617&scope=bot)  

* **클릭시 봇 초대 링크로 이동**  

---

```plaintext
M4D3 I3Y 최박사 | D0C70R_CH01 | TWITTER @d0c70r_ch01 | DISCORD 최박사 | D0C70R_CH01#2885

https://discord.com/oauth2/authorize?client_id=APPLICATION ID&permissions=36719617&scope=bot
permission  36719617

recommend node.js version  v20.10.0

[2024-05-27 05:14] release v2_hf4
[2024-03-15 □□:□□] release v2_hf3
[2024-02-26 □□:□□] release v2_hf2
[2024-02-26 □□:□□] release v2_hf1
[2024-02-26 07:09] release v2

[2023-10-18 20:15] release v1_hf3
[2023-06-06 21:06] release v1_hf2
[2023-06-06 02:26] release v1_hf1
[2023-05-17 04:00] release v1

[2023-03-10 04:14] release alpha3_minor2
[2023-03-09 03:33] release alpha3_minor1
[2023-02-10 07:30] release alpha3
[2022-08-23 03:39] release alpha2
[2022-08-13 04:38] release alpha1_hf1
[2022-07-28 05:43] release alpha1
```

> 최박사 : 얘 맛이 갔는데요?  
> □□□ : 어이 개발자양반, 고쳐보슈  

---

* [최초 사용시 가이드로 이동](#최초-사용시-가이드)  
* [명령어 목록으로 이동](#명령어)  
* [변경점 로그로 이동](./CHANGELOG.md)

---

## 최초 사용시 가이드  

1. [봇 생성](#봇-생성)  

2. [구글 TTS 활성화](#구글-tts-활성화)  

3. [터미널 설정](#터미널-설정)  

### 봇 생성  

<https://discord.com/developers/applications>  
으로 이동해서 로그인합니다.  

![생성](./img/NewApplication.png)  
우상단의 New Application 버튼을 누릅니다.  

![이름 입력](./img/botName.png)  
봇 이름을 입력합니다.  

![봇 정보](./img/botInfo.png)  
봇 설명을 적습니다.(선택사항)  
밑의 APPLICATION ID는 나중에 봇을 서버에 초대할 때 사용합니다.  

![봇 설정](./img/botSetting.png)  
왼쪽 메뉴에서 Bot 을 찾아 선택합니다.  

![봇 빌드](./img/buildABot.png)  
오른쪽의 Add Bot 버튼을 누릅니다.

![Yes, Do It!](./img/yesDoIt.png)  
오른쪽 버튼을 누릅니다.  

![아이콘과 이름 설정](./img/botIcon.png)  
봇 아이콘과 이름을 설정합니다.  

![봇 정보2](./img/botInfo2.png)  
디스코드 안에서는 이런 식으로 표시됩니다.  

![토큰 리셋](./img/resetToken.png)  
Reset Token 버튼을 누릅니다.  

![토큰](./img/token.png)  
왼쪽의 Copy 버튼을 누릅니다.  

![토큰2](./img/token2.png)  
압축 푼 폴더 안에 token (확장자 없음, 파일명 token) 파일을 만들고,  
아까 복사했던 토큰을 붙여넣고 저장합니다.  

---

### 구글 TTS 활성화  

<https://cloud.google.com/>  
으로 이동해서 로그인합니다.  

![콘솔](./img/console.png)  
콘솔로 이동합니다.  

![정책](./img/policies.png)  
동의합니다.  

![프로젝트 선택](./img/projectSelect.png)  
좌상단의 프로젝트 선택 버튼을 누릅니다.  

![새 프로젝트](./img/newProject.png)  
새 프로젝트 버튼을 누릅니다.

![프로젝트 이름](./img/projectName.png)  
프로젝트 이름을 정하고 만들기 버튼을 누릅니다.  

![프로젝트 선택2](./img/projectSelect2.png)  
생성한 프로젝트를 선택합니다.  

![API](./img/API.png)  
왼쪽 메뉴에서 API 및 서비스 버튼을 누릅니다.

![API2](./img/API2.png)  
API 및 서비스 사용 설정 버튼을 누릅니다.  

![tts](./img/tts.png)  
text-to-speech 검색을 합니다.

![tts2](./img/tts2.png)  
Cloud Text-to-Speech API 를 찾습니다.  

![tts3](./img/tts3.png)  
사용 버튼을 누릅니다.  

![결제](./img/billing.png)  
결제 설정을 합니다. (실제 사용되는 돈 0원)  

![결제2](./img/billing2.png)  
![결제3](./img/billing3.png)  
![결제4](./img/billing4.png)  
![결제5](./img/billing5.png)  
![결제6](./img/billing6.png)  
![결제7](./img/billing7.png)  
시키는 대로 합니다.  

![결제8](./img/billing8.png)  
건너뛰기 버튼을 누릅니다.  

![tts3.99](./img/tts3.png)  
다시 사용 버튼을 누릅니다.  

![tts4](./img/tts4.png)  
이렇게 tts 봇을 사용하도록 설정합니다.  

![IAM](./img/IAM.png)  
왼쪽 메뉴에서 IAM 및 관리자 버튼을 누릅니다.

![서비스 계정](./img/serviceAccount.png)  
왼쪽 메뉴에서 서비스 계정 버튼을 누릅니다.  

![서비스 계정2](./img/serviceAccount2.png)  
서비스 계정 만들기 버튼을 누릅니다.  

![서비스 계정3](./img/serviceAccount3.png)  
시키는 대로 합니다.

![키](./img/key.png)  
만들어진 서비스 계정을 누르고 키 항목을 누릅니다.  

![키2](./img/key2.png)  
키 추가 버튼을 누르고 만들기 버튼을 누릅니다.  

![키3](./img/key3.png)  
json 파일을 안전한 곳에 저장합니다.

---

### 터미널 설정  

![터미널](./img/terminal.png)  
봇을 작동시킬 PowerShell을 켭니다.  

```powershell
$env:GOOGLE_APPLICATION_CREDENTIALS="아까 저장한 json 키"
```  

입력합니다.

```powershell
PS C:\Users\D0C70R_CH01\Downloads\doctor_choi_the_reader-main> tsc
PS C:\Users\D0C70R_CH01\Downloads\doctor_choi_the_reader-main> node ./out/index.js
[21:13:08.103] [LOG] Hello,World!
[21:13:08.105] [WARN] Critical Error
[21:13:08.105] [ERROR] Ion Efflux
[21:13:08.107] [ERROR] (node:24748) ExperimentalWarning: stream/web is an experimental feature. This feature could change at any time
(Use `node --trace-warnings ...` to show where the warning was created)
[21:13:08.955] [LOG] 최박사, 읽는 자#8156 : ON
```

작동시킵니다.  

![봇 설치](./img/botCommand.png)  
채팅을 읽을 텍스트 채널에 설치 명령어를 사용합니다.  

![봇 입장](./img/botCommand2.png)  
음성 채널로 입장한 뒤, 봇을 입장시킵니다.  

![봇 입장2](./img/botJoin.png)  
봇을 입장시켰으면 설치 명령어를 사용한 채널에 채팅을 치면 봇이 읽어줍니다.

---

## 명령어  

> 기본 접두사 : **?**  

* [정보](#정보--info)  
* [핑](#핑--ping)  
* [설치](#설치--install)  
* [제거](#제거--uninstall)  
* [입장](#입장--join)  
* [퇴장](#퇴장--leave)  
* [스킵](#스킵--skip)  
* [커피](#커피--coffee)  

---

### 정보 / info

![정보](./img/info.png)  

* 봇의 정보를 출력합니다.  

---

### 핑 / ping  

![핑](./img/ping.png)  

* 지연시간을 출력합니다.  

---

### 설치 / install  

![설치](./img/install.png)  

* 봇이 텍스트를 읽을 **텍스트 채널**을 지정합니다.  
* 한번 지정한 이후에도 변경이 가능합니다.  
* 지정되어있지 않으면 이하의 명령어가 작동하지 않습니다.

---

### 제거 / uninstall  

![제거](./img/uninstall.png)  

* 봇이 텍스트를 읽을 텍스트 채널을 제거합니다.

---

### 입장 / join  

![입장](./img/join.png)  

* 봇을 **음성 채널**에 입장시킵니다.  
* 음성 채널에 입장한 상태로 실행시켜야 작동합니다.  
* 잠수 채널의 경우 입장하지 않습니다.  

---

### 퇴장 / leave  

![퇴장](./img/leave.png)  

* 봇을 음성 채널에서 퇴장시킵니다.

---

### 스킵 / skip  

![스킵](./img/skip.png)  

* 읽던 문구를 즉시 멈춥니다.  

---

### 커피 / coffee  

![커피](./img/coffee.png)  

* 가난한 최박사에게 커피를 사주세요.  

---
---
---
---
