"use strict";
/* CODE I3Y D0C70R_CH01 (https://gitlab.com/D0C70R_CH01) */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.csessions = exports.chats = void 0;
const discordVoice = __importStar(require("@discordjs/voice"));
const textToSpeech = __importStar(require("@google-cloud/text-to-speech"));
const fs_1 = __importDefault(require("fs"));
// import 끗
class chats extends Object {
    constructor(_name, _chat) {
        super();
        this.name = _name;
        this.chat = _chat;
    }
}
exports.chats = chats;
// chats 선언 끗
class csessions extends Object {
    // public 선언 끗
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    _fread() {
        this.timeout = setTimeout(() => { this._fread(); }, 20);
        // this.timeout2 = setTimeout(() => { this._fname() }, 1000 * 60 * 5);
        // 일단 타임아웃 재설정
        if (this.player.state.status == discordVoice.AudioPlayerStatus.Playing)
            return;
        if (this.joined == null)
            return;
        if (this.joinedid == undefined)
            return;
        if (this.chatQueue.length == 0)
            return;
        // 쓸때없는 연산 금지
        var content;
        // 변수선언 끗
        if (this.last == this.chatQueue[0].name) {
            content = this.chatQueue[0].chat;
        }
        else {
            if (this.chatQueue[0].chat.match(/^[?!'.,]+$/)) {
                this.chatQueue[0].chat = this.chatQueue[0].chat.replace(/[?]/g, "물음표 ");
                this.chatQueue[0].chat = this.chatQueue[0].chat.replace(/[!]/g, "느낌표 ");
                this.chatQueue[0].chat = this.chatQueue[0].chat.replace(/[']/g, "아포스트로피 ");
                this.chatQueue[0].chat = this.chatQueue[0].chat.replace(/[.]/g, "점 ");
                this.chatQueue[0].chat = this.chatQueue[0].chat.replace(/[,]/g, "쉼표 ");
            }
            // 안읽는거 읽게하기
            content = this.chatQueue[0].name + this.__SAID + this.chatQueue[0].chat;
        }
        this.last = this.chatQueue[0].name;
        content = content.replace(/(http|https|ftp|telnet|news|mms):\/\/[^\"'\s()]+/gi, "링크");
        // 링크 주소 치환
        var req = {
            input: { text: content },
            voice: this.__VOICE,
            audioConfig: { audioEncoding: "MP3" }
        };
        try {
            this.__TTS.synthesizeSpeech(req, (e, res) => {
                if (e) {
                    console.error(e);
                    this.timeout = setTimeout(() => { this._fread(); }, 20);
                    // 타임아웃 재설정
                    return;
                }
                // 에러같은거 있으면 리턴
                this.timeout = setTimeout(() => { this._fread(); }, 1100);
                // 채팅 읽는 시간을 위하여 타임아웃을 길게 잡음
                try {
                    fs_1.default.writeFileSync("./tts/" + this.joinedid + "_buffer.mp3", res.audioContent);
                }
                catch (e) {
                    console.error(e);
                    this.timeout = setTimeout(() => { this._fread(); }, 20);
                    // 타임아웃 재설정
                    return;
                    // 에러생기면 리턴
                    // 임시 코드
                }
                this.player.play(discordVoice.createAudioResource("./tts/" + this.joinedid + "_buffer.mp3")); // 이거 왜 에러
                this.chatQueue.shift();
                // tts 재생
                console.log("readed!");
            });
            // 텍스트 읽는 함수 끗   
        }
        catch (e) {
            console.error("TTS 생성 실패");
            this.timeout = setTimeout(() => { this._fread(); }, 20);
            return;
        }
        // tts 생성
        clearTimeout(this.timeout);
        // 잠시 타임아웃 제거
        console.log(content + " is maked");
        // 디버그용
    }
    _fname() {
        this.last = "";
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    constructor(guild) {
        super();
        this.__SAID = "  said | ";
        this.__VOICE = { languageCode: "ko_KR", ssmlGender: "FEMALE", name: "ko-KR-Wavenet-A" };
        this.__GUILD = undefined;
        this.__TTS = new textToSpeech.TextToSpeechClient();
        // private 선언 끗
        this.chatQueue = [];
        this.joined = undefined;
        this.joinedid = String(Math.random() * 10000000);
        this.last = "";
        this.player = discordVoice.createAudioPlayer({ behaviors: { noSubscriber: discordVoice.NoSubscriberBehavior.Play } });
        this.timeout = undefined;
        this.timeout2 = undefined;
        this.__GUILD = guild;
    }
    // 생성자 끗
    guild() {
        return this.__GUILD;
    }
    // 길드 투척하는 함수 끗
    add(chat) {
        this.chatQueue.push(chat);
    }
    // 추가 함수 끗
    run() {
        if (this.joined != undefined)
            this.joined.subscribe(this.player);
        this._fread();
    }
    // 설치 완료후 작동 함수 끗
    stop() {
        clearTimeout(this.timeout);
    }
}
exports.csessions = csessions;
// 세션 관련 클래스 선언 끗
