"use strict";
/* CODE I3Y D0C70R_CH01 (https://gitlab.com/D0C70R_CH01) */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const textToSpeech = __importStar(require("@google-cloud/text-to-speech"));
const fs_1 = __importDefault(require("fs"));
// import getAudioDurationInSeconds from "get-audio-duration";
// import 끗
const __TEXT = "최박사 | D0C70R_CH01 said | 테스트용문구입니다람쥐썬더";
var client = new textToSpeech.TextToSpeechClient();
var req = {
    input: { text: __TEXT },
    voice: { languageCode: "ko_KR", ssmlGender: "FEMALE", name: "ko-KR-Wavenet-A" },
    audioConfig: { audioEncoding: "MP3" }
};
client.synthesizeSpeech(req, ftts);
function ftts(e, res) {
    if (e) {
        console.error(e);
        return;
    }
    fs_1.default.writeFileSync("./tts/buffer.mp3", res.audioContent);
    // getAudioDurationInSeconds("./tts/buffer.mp3").then((dulation: number) => { console.log(dulation) });
}
