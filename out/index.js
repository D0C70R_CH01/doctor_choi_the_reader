"use strict";
/* CODE I3Y D0C70R_CH01 (https://gitlab.com/D0C70R_CH01) */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_1 = __importDefault(require("discord.js"));
const discordVoice = __importStar(require("@discordjs/voice"));
const fs_1 = __importDefault(require("fs"));
const console_stamp_1 = __importDefault(require("console-stamp"));
const sessions = __importStar(require("./lib/csessons"));
// import 끗
const __VERSION = "release v2_hf4";
const __PREFIX = "?";
// 상수선언 끗
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
/* 세션 관련 변수들 */
var sess = new Array;
var list;
/* 세션이 있으면 복원 */
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
var _cs = (0, console_stamp_1.default)(console, { format: ":date(HH:MM:ss.l) :label" });
var token = fs_1.default.readFileSync("token", "utf-8");
var client = new discord_js_1.default.Client({ intents: ["Guilds", "GuildMessages", "MessageContent", "GuildVoiceStates"] });
// 변수선언 끗
console.log("Hello,World!");
console.warn("Critical Error");
console.error("Ion Efflux");
// 헬로월드 발사
function ferror1(msg) {
    var _a;
    msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
        embeds: [
            {
                author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                color: 0xC00000,
                description: "TTS봇을 설치해 주세요.",
                footer: { text: msg.content },
                title: "ERROR (-1)",
                thumbnail: { url: String((_a = client.user) === null || _a === void 0 ? void 0 : _a.avatarURL()) }
            }
        ]
    })).catch(console.error);
    console.warn(msg.author.tag + " : " + msg.content + " is ERROR -1");
}
//error -1 던지는 함수 끗
function fmakeTs() {
    var ts = "[";
    //[1d 12:34:56:789]
    var tmp = client.uptime;
    ts += (tmp / 1000 / 60 / 60 / 24).toFixed() + "d ";
    ts += (tmp / 1000 / 60 / 60 % 24).toFixed().padStart(2, "0") + ":";
    ts += (tmp / 1000 / 60 % 60).toFixed().padStart(2, "0") + ":";
    ts += (tmp / 1000 % 60).toFixed().padStart(2, "0") + ":";
    ts += (tmp % 1000).toFixed().padStart(3, "0") + "]";
    return ts;
}
// 업타임 함수 끗
client.login(token);
// 봇 업
client.on("ready", fready);
function fready() {
    var _a, _b;
    return __awaiter(this, void 0, void 0, function* () {
        console.log("client.ready");
        console.log(((_a = client.user) === null || _a === void 0 ? void 0 : _a.tag) + " : ON");
        (_b = client.user) === null || _b === void 0 ? void 0 : _b.setActivity(__PREFIX + "정보");
        try {
            list = JSON.parse(fs_1.default.readFileSync("./backup.doctorchoithereader", { encoding: "utf-8" }));
            for (var i = 0; i < list.length; i++) {
                var tmp = new sessions.csessions(yield client.guilds.fetch(list[i].guild));
                // 내가 이겼다
                if (list[i].ttsroom) {
                    tmp.guild().channels.fetch(list[i].ttsroom)
                        .then(function (channels) {
                        var _a;
                        if ((channels === null || channels === void 0 ? void 0 : channels.type) === discord_js_1.default.ChannelType.GuildText) {
                            tmp.ttsroom = channels;
                            channels.send(new discord_js_1.default.MessagePayload(channels, {
                                embeds: [
                                    {
                                        author: { name: client.user.username, icon_url: String(client.user.avatarURL()) },
                                        color: 0x0080C0,
                                        description: "최박사 TTS봇 서버 복구 완료!\n명령어를 사용하여 음성 채널에 봇을 입장시키세요.",
                                        footer: { text: "서버 복구, VERSION " + __VERSION },
                                        title: "서버 복구 완료!",
                                        thumbnail: { url: String((_a = client.user) === null || _a === void 0 ? void 0 : _a.avatarURL()) }
                                    }
                                ]
                            })).catch(console.error);
                        }
                    })
                        .catch(console.error);
                }
                sess.push(tmp);
            }
        }
        catch (e) {
            console.error(e);
        }
        console.log("전체 세션 복원 완료");
    });
}
// 봇 작동시 함수 끗
client.on("invalidated", fautoReconnect);
function fautoReconnect() {
    console.warn("client.invalidated");
    client.login(token);
}
// 자동 재연결 함수 끗
client.on("error", fclientError);
function fclientError(e) {
    console.error("client.error");
    console.error(e.name);
    console.error(e.message);
}
// 에러 함수 끗
client.on("warn", fclientWarn);
function fclientWarn(e) {
    console.warn("client.warn");
    console.warn(e);
}
// 경고 함수 끗
client.on("messageCreate", fmessage);
function fmessage(msg) {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u, _v, _w, _x, _y, _z, _0, _1, _2, _3, _4, _5;
    if (msg.author.bot)
        return;
    // 쓸때없는 연산 금지
    console.log("event : messageCreate");
    (_a = client.user) === null || _a === void 0 ? void 0 : _a.setActivity(__PREFIX + "정보");
    var nowIndex = -4444;
    if (sess.length != 0)
        for (var i = 0; i < sess.length; i++) {
            // console.log(sess.length + " " + i);
            // console.log(sess[i].guild().name + " " + msg.guild?.name);
            if (sess[i].guild().name === ((_b = msg.guild) === null || _b === void 0 ? void 0 : _b.name)) {
                nowIndex = i;
                console.log("found session : " + sess[nowIndex].guild().name);
                break;
            }
        }
    // 세션 찾기
    if (nowIndex == -4444) {
        console.log("new session : " + sess[sess.push(new sessions.csessions(msg.guild)) - 1].guild().name);
        if (msg.guild)
            list.push({ guild: msg.guild.id, ttsroom: undefined });
        // 세션 추가
        for (var i = 0; i < sess.length; i++) {
            if (sess[i].guild().name === ((_c = msg.guild) === null || _c === void 0 ? void 0 : _c.name)) {
                nowIndex = i;
                fs_1.default.writeFile("./backup.doctorchoithereader", JSON.stringify(list), { encoding: "utf-8" }, function (e) {
                    if (e)
                        console.error(e);
                    else
                        console.log("전체 세션 백업 완료");
                });
                // @todo 전체 세션 백업
                break;
            }
        }
        // 이후 세션을 한번 새로 찾는다
    }
    if (msg.content.startsWith("?!??"))
        return;
    // TTS로 안읽게 하기 접두
    if (msg.content.startsWith(__PREFIX)) {
        var command = msg.content.slice(__PREFIX.length);
        var args = command.split(' ');
        // 변수선언 끗
        args[0] = args[0].toLocaleLowerCase();
        // 코드 편의를 위해서
        if (args.length == 1)
            switch (args[0]) 
            // 명령어 처리 시작
            {
                case "정보":
                case " info":
                    console.log(msg.author.tag + " : " + msg.content);
                    msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                        embeds: [
                            {
                                author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                color: 0x0080C0,
                                description: "https://gitlab.com/D0C70R_CH01/doctor_choi_the_reader  \nM4D3 I3Y 최박사 | D0C70R_CH01  \nTWITTER : @d0c70r_ch01  \nDISCORD : d0c70r_ch01  \n업타임 : " + fmakeTs() + "\n이 봇을 사용중인 서버 : " + sess.length + "개 \n\n명령어는 git의 README.md 참조  \n버그문의는 git에 이슈 달거나 트위터, 디스코드 디엠  \n\nVERSION " + __VERSION,
                                footer: { text: msg.content },
                                title: "봇 정보",
                                thumbnail: { url: String((_d = client.user) === null || _d === void 0 ? void 0 : _d.avatarURL()) }
                            }
                        ]
                    })).catch(console.error);
                    return;
                ////////////////////////////////////////////////////////////////////////////////////////////////
                //
                // 정보
                //
                ////////////////////////////////////////////////////////////////////////////////////////////////
                case "핑":
                case "ping":
                    console.log(msg.author.tag + " : " + msg.content);
                    msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                        embeds: [
                            {
                                author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                color: 0x0080C0,
                                description: client.ws.ping + "ms",
                                footer: { text: msg.content },
                                title: "서버 딜레이",
                                thumbnail: { url: String((_e = client.user) === null || _e === void 0 ? void 0 : _e.avatarURL()) }
                            }
                        ]
                    })).catch(console.error);
                    return;
                ////////////////////////////////////////////////////////////////////////////////////////////////
                //
                // 핑
                //
                ////////////////////////////////////////////////////////////////////////////////////////////////
                case "설치":
                case "install":
                    console.log(msg.author.tag + " : " + msg.content);
                    try {
                        // ttsRoom = msg.channel;
                        // chatQueue = [];
                        if (msg.channel.type === discord_js_1.default.ChannelType.GuildText) {
                            sess[nowIndex].ttsroom = msg.channel;
                            sess[nowIndex].chatQueue = [];
                            list[nowIndex].ttsroom = msg.channelId;
                        }
                    }
                    catch (e) {
                        msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                            embeds: [
                                {
                                    author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                    color: 0xC00000,
                                    description: "TTS봇 설치에 실패하였습니다.\n" + e.toString(),
                                    footer: { text: msg.content },
                                    title: "ERROR (-2)",
                                    thumbnail: { url: String((_f = client.user) === null || _f === void 0 ? void 0 : _f.avatarURL()) }
                                }
                            ]
                        })).catch(console.error);
                        console.warn(msg.author.tag + " : " + msg.content + " is ERROR -2");
                        return;
                    }
                    msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                        embeds: [
                            {
                                author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                color: 0x0080C0,
                                description: "명령어를 사용하여 음성 채널에 봇을 입장시키세요.",
                                footer: { text: msg.content },
                                title: "TTS봇 설치 완료!",
                                thumbnail: { url: String((_g = client.user) === null || _g === void 0 ? void 0 : _g.avatarURL()) }
                            }
                        ]
                    })).catch(console.error);
                    fs_1.default.writeFile("./backup.doctorchoithereader", JSON.stringify(list), { encoding: "utf-8" }, function (e) {
                        if (e)
                            console.error(e);
                        else
                            console.log("전체 세션 백업 완료");
                    });
                    // 전체 세션 백업
                    return;
                ////////////////////////////////////////////////////////////////////////////////////////////////
                //
                // 설치
                //
                ////////////////////////////////////////////////////////////////////////////////////////////////
                case "제거":
                case "uninstall":
                    console.log(msg.author.tag + " : " + msg.content);
                    if (sess[nowIndex] == undefined) {
                        ferror1(msg);
                        return;
                    }
                    //설치가 안되어있으면 종료
                    if (msg.channel.id != ((_h = sess[nowIndex].ttsroom) === null || _h === void 0 ? void 0 : _h.id)) {
                        console.warn("debug : 1");
                        return;
                    }
                    //설치된 채널이 아니면 종료
                    try {
                        if (sess[nowIndex].joined == undefined)
                            return;
                        sess[nowIndex].player.stop(true);
                        (_j = sess[nowIndex].joined) === null || _j === void 0 ? void 0 : _j.destroy();
                        sess[nowIndex].joined = undefined;
                        sess[nowIndex].stop();
                    }
                    catch (e) {
                        msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                            embeds: [
                                {
                                    author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                    color: 0xC00000,
                                    description: "TTS봇 퇴장에 실패하였습니다.\n" + e.toString(),
                                    footer: { text: msg.content },
                                    title: "ERROR (-5)",
                                    thumbnail: { url: String((_k = client.user) === null || _k === void 0 ? void 0 : _k.avatarURL()) }
                                }
                            ]
                        })).catch(console.error);
                    }
                    // 음성방에 들어가 있으면 먼저 퇴장을 시도한다
                    try {
                        sess[nowIndex].ttsroom = undefined;
                        list[nowIndex].ttsroom = undefined;
                    }
                    catch (e) {
                        msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                            embeds: [
                                {
                                    author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                    color: 0xC00000,
                                    description: "TTS봇 제거에 실패하였습니다.\n" + e.toString(),
                                    footer: { text: msg.content },
                                    title: "ERROR (-3)",
                                    thumbnail: { url: String((_l = client.user) === null || _l === void 0 ? void 0 : _l.avatarURL()) }
                                }
                            ]
                        })).catch(console.error);
                        console.warn(msg.author.tag + " : " + msg.content + " is ERROR -3");
                        return;
                    }
                    msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                        embeds: [
                            {
                                author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                color: 0x0080C0,
                                description: "더이상 봇이 이 채팅방을 읽지 않습니다.",
                                footer: { text: msg.content },
                                title: "TTS봇 제거 완료!",
                                thumbnail: { url: String((_m = client.user) === null || _m === void 0 ? void 0 : _m.avatarURL()) }
                            }
                        ]
                    })).catch(console.error);
                    fs_1.default.writeFile("./backup.doctorchoithereader", JSON.stringify(list), { encoding: "utf-8" }, function (e) {
                        if (e)
                            console.error(e);
                        else
                            console.log("전체 세션 백업 완료");
                    });
                    // 전체 세션 백업
                    return;
                ////////////////////////////////////////////////////////////////////////////////////////////////
                //
                // 제거
                //
                ////////////////////////////////////////////////////////////////////////////////////////////////
                case "입장":
                case "join":
                    console.log(msg.author.tag + " : " + msg.content);
                    if (sess[nowIndex] == undefined) {
                        ferror1(msg);
                        return;
                    }
                    //설치가 안되어있으면 종료
                    if (msg.channel != sess[nowIndex].ttsroom) {
                        ferror1(msg);
                        return;
                    }
                    //설치된 채널이 아니면 종료
                    try {
                        if (((_o = msg.member) === null || _o === void 0 ? void 0 : _o.voice.channelId) == undefined || ((_p = msg.member) === null || _p === void 0 ? void 0 : _p.voice.channelId) == null)
                            throw new Error("음성 채팅에 먼저 입장해 주세요.");
                        if (((_q = msg.member) === null || _q === void 0 ? void 0 : _q.voice.channelId) == ((_s = (_r = msg.guild) === null || _r === void 0 ? void 0 : _r.afkChannel) === null || _s === void 0 ? void 0 : _s.id))
                            throw new Error("잠수 채널에는 참여할 수 없습니다.");
                        if (sess[nowIndex].joined != undefined)
                            throw new Error("이미 음성 채널에 입장되어 있습니다.");
                        sess[nowIndex].joinedid = msg.member.voice.channelId;
                        sess[nowIndex].joined = discordVoice.joinVoiceChannel({ channelId: (_t = msg.member) === null || _t === void 0 ? void 0 : _t.voice.channelId, guildId: (_u = msg.guild) === null || _u === void 0 ? void 0 : _u.id, adapterCreator: (_v = msg.guild) === null || _v === void 0 ? void 0 : _v.voiceAdapterCreator, selfDeaf: false, selfMute: false });
                        // sess[nowIndex].joined?.subscribe(sess[nowIndex].player);
                        // // 이거 왜 undefined로 인식하냐?
                        sess[nowIndex].chatQueue = [];
                        sess[nowIndex].run();
                    }
                    catch (e) {
                        msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                            embeds: [
                                {
                                    author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                    color: 0xC00000,
                                    description: "TTS봇 입장에 실패하였습니다.\n" + e.toString(),
                                    footer: { text: msg.content },
                                    title: "ERROR (-4)",
                                    thumbnail: { url: String((_w = client.user) === null || _w === void 0 ? void 0 : _w.avatarURL()) }
                                }
                            ]
                        })).catch(console.error);
                        console.warn(msg.author.tag + " : " + msg.content + " is ERROR -4");
                        return;
                    }
                    msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                        embeds: [
                            {
                                author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                color: 0x0080C0,
                                description: "채팅을 치면 봇이 채팅을 읽어 줍니다.",
                                footer: { text: msg.content },
                                title: "TTS봇 입장",
                                thumbnail: { url: String((_x = client.user) === null || _x === void 0 ? void 0 : _x.avatarURL()) }
                            }
                        ]
                    })).catch(console.error);
                    return;
                ////////////////////////////////////////////////////////////////////////////////////////////////
                //
                // 입장
                //
                ////////////////////////////////////////////////////////////////////////////////////////////////
                case "퇴장":
                case "leave":
                    if (sess[nowIndex] == undefined) {
                        ferror1(msg);
                        return;
                    }
                    //설치가 안되어있으면 종료
                    console.log(msg.author.tag + " : " + msg.content);
                    if (sess[nowIndex].ttsroom == undefined) {
                        ferror1(msg);
                        return;
                    }
                    //설치가 안되어있으면 종료
                    if (msg.channel != sess[nowIndex].ttsroom)
                        return;
                    //설치된 채널이 아니면 종료
                    try {
                        if (sess[nowIndex].joined == undefined)
                            return;
                        sess[nowIndex].player.stop(true);
                        (_y = sess[nowIndex].joined) === null || _y === void 0 ? void 0 : _y.destroy();
                        sess[nowIndex].joined = undefined;
                        sess[nowIndex].stop();
                    }
                    catch (e) {
                        msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                            embeds: [
                                {
                                    author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                    color: 0xC00000,
                                    description: "TTS봇 퇴장에 실패하였습니다.\n" + e.toString(),
                                    footer: { text: msg.content },
                                    title: "ERROR (-5)",
                                    thumbnail: { url: String((_z = client.user) === null || _z === void 0 ? void 0 : _z.avatarURL()) }
                                }
                            ]
                        })).catch(console.error);
                        console.warn(msg.author.tag + " : " + msg.content + " is ERROR -5");
                        return;
                    }
                    msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                        embeds: [
                            {
                                author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                color: 0x0080C0,
                                description: "더이상 봇이 채팅을 읽어주지 않습니다.",
                                footer: { text: msg.content },
                                title: "TTS봇 퇴장",
                                thumbnail: { url: String((_0 = client.user) === null || _0 === void 0 ? void 0 : _0.avatarURL()) }
                            }
                        ]
                    })).catch(console.error);
                    sess[nowIndex].last = "";
                    sess[nowIndex].chatQueue = [];
                    return;
                ////////////////////////////////////////////////////////////////////////////////////////////////
                //
                // 퇴장
                //
                ////////////////////////////////////////////////////////////////////////////////////////////////
                case "스킵":
                case "skip":
                    console.log(msg.author.tag + " : " + msg.content);
                    if (sess[nowIndex] == undefined) {
                        ferror1(msg);
                        return;
                    }
                    //설치가 안되어있으면 종료
                    if (msg.channel != sess[nowIndex].ttsroom)
                        return;
                    //설치된 채널이 아니면 종료
                    if (sess[nowIndex].joined == undefined)
                        return;
                    //음성방에 봇이 없으면 종료
                    try {
                        sess[nowIndex].player.stop();
                    }
                    catch (e) {
                        msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                            embeds: [
                                {
                                    author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                    color: 0xC00000,
                                    description: "스킵에 실패하였습니다.\n" + e.toString(),
                                    footer: { text: msg.content },
                                    title: "ERROR (-6)",
                                    thumbnail: { url: String((_1 = client.user) === null || _1 === void 0 ? void 0 : _1.avatarURL()) }
                                }
                            ]
                        })).catch(console.error);
                        console.warn(msg.author.tag + " : " + msg.content + " is ERROR -6");
                        return;
                    }
                    msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                        embeds: [
                            {
                                author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                color: 0x0080C0,
                                description: "쉿!",
                                footer: { text: msg.content },
                                title: "스킵",
                                thumbnail: { url: String((_2 = client.user) === null || _2 === void 0 ? void 0 : _2.avatarURL()) }
                            }
                        ]
                    })).catch(console.error);
                    return;
                ////////////////////////////////////////////////////////////////////////////////////////////////
                //
                // 스킵
                //
                ////////////////////////////////////////////////////////////////////////////////////////////////
                case "커피":
                case "coffee":
                    msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                        embeds: [
                            {
                                author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                color: 0x0080C0,
                                description: "가난한 최박사에게 커피를 사주세요.\nhttps://www.buymeacoffee.com/d0c70rch01",
                                footer: { text: msg.content },
                                title: "커피",
                                thumbnail: { url: String((_3 = client.user) === null || _3 === void 0 ? void 0 : _3.avatarURL()) },
                                url: "https://www.buymeacoffee.com/d0c70rch01"
                            }
                        ]
                    })).catch(console.error);
                    return;
                ////////////////////////////////////////////////////////////////////////////////////////////////
                //
                // 커피
                //
                ////////////////////////////////////////////////////////////////////////////////////////////////
            }
        //명령어 처리
    }
    // 명령어 접두로 시작할 시
    if (sess[nowIndex] == undefined)
        return;
    // 세션이 없으면 바로 리턴
    if (msg.channel == sess[nowIndex].ttsroom) {
        if (sess[nowIndex].ttsroom == undefined || sess[nowIndex].joined == undefined)
            return;
        if ((((_4 = msg.member) === null || _4 === void 0 ? void 0 : _4.voice.channelId) != undefined || ((_5 = msg.member) === null || _5 === void 0 ? void 0 : _5.voice.channelId) != null) && sess[nowIndex].ttsroom != undefined && msg.member.voice.channelId == sess[nowIndex].joinedid) {
            // chatQueue.push({name: msg.member.displayName, chat: msg.cleanContent});
            sess[nowIndex].add(new sessions.chats(msg.member.displayName, msg.cleanContent));
            console.log(msg.author.tag + " " + msg.cleanContent + " is added");
        }
        // 아닌 경우
    }
    // console.log(chatQueue);
    // console.log(sess[0]);
    // fs.writeFile("doctorchoi_the_reader.sessionbackup", JSON.stringify(sess), function(err : NodeJS.ErrnoException | null) { if(err) console.error(err); } );
    // 세션 백업
}
//메세지 처리 함수 끗
