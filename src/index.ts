/* CODE I3Y D0C70R_CH01 (https://gitlab.com/D0C70R_CH01) */

import discord from "discord.js";
import * as discordVoice from "@discordjs/voice";
import fs from "fs";
import consoleStamp from "console-stamp";

import * as sessions from "./lib/csessons";
// import 끗

const __VERSION: string = "release v2_hf4";
const __PREFIX: string = "?";
// 상수선언 끗

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

/* 세션 관련 변수들 */

var sess: Array<sessions.csessions> = new Array<sessions.csessions>;
var list: Array<{guild: string, ttsroom: string | undefined}>;
    /* 세션이 있으면 복원 */

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

var _cs = consoleStamp(console, {format: ":date(HH:MM:ss.l) :label"});
var token = fs.readFileSync("token", "utf-8");
var client: discord.Client = new discord.Client({intents: ["Guilds", "GuildMessages", "MessageContent", "GuildVoiceStates"]});
// 변수선언 끗

console.log("Hello,World!");
console.warn("Critical Error");
console.error("Ion Efflux");
// 헬로월드 발사

function ferror1(msg: discord.Message)
{
    msg.channel.send(new discord.MessagePayload(msg.channel, 
        {
            embeds: [
            {
                author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                color: 0xC00000, 
                description: "TTS봇을 설치해 주세요.", 
                footer: {text: msg.content}, 
                title: "ERROR (-1)", 
                thumbnail: {url: String(client.user?.avatarURL())}
            }]
        })).catch(console.error);

        console.warn(msg.author.tag + " : " + msg.content + " is ERROR -1")
}
//error -1 던지는 함수 끗

function fmakeTs(): string
{
    var ts: string = "[";
    //[1d 12:34:56:789]

    var tmp : number = client.uptime as number;
    ts += (tmp / 1000 / 60 / 60 /24).toFixed() + "d ";
    ts += (tmp / 1000 / 60 / 60 % 24).toFixed().padStart(2, "0") + ":";
    ts += (tmp / 1000 / 60 % 60).toFixed().padStart(2, "0") + ":";
    ts += (tmp / 1000 % 60).toFixed().padStart(2, "0") + ":";
    ts += (tmp % 1000).toFixed().padStart(3, "0") + "]";

    return ts;
}
// 업타임 함수 끗

client.login(token);
// 봇 업

client.on("ready", fready);
async function fready(): Promise<void>
{
    console.log("client.ready");
    console.log(client.user?.tag + " : ON");

    client.user?.setActivity(__PREFIX + "정보");

    try
    {
        list = JSON.parse(fs.readFileSync("./backup.doctorchoithereader", {encoding: "utf-8"}));

        for(var i: number = 0; i < list.length; i++)
        {
            var tmp: sessions.csessions = new sessions.csessions(await client.guilds.fetch(list[i].guild));
            // 내가 이겼다

            if(list[i].ttsroom)
            {
                tmp.guild().channels.fetch(list[i].ttsroom as string)
                .then(function (channels: discord.GuildBasedChannel | null): void
                    {
                        if(channels?.type === discord.ChannelType.GuildText)
                        {
                            tmp.ttsroom = channels;

                            channels.send(new discord.MessagePayload(channels, 
                            {
                                embeds: [
                                {
                                    author: {name: client.user!.username, icon_url: String(client.user!.avatarURL())}, 
                                    color: 0x0080C0, 
                                    description: "최박사 TTS봇 서버 복구 완료!\n명령어를 사용하여 음성 채널에 봇을 입장시키세요.", 
                                    footer: {text: "서버 복구, VERSION " + __VERSION}, 
                                    title: "서버 복구 완료!", 
                                    thumbnail: {url: String(client.user?.avatarURL())}
                                }]
                            })).catch(console.error);
                        }
                    })
                    .catch(console.error);
            }

            sess.push(tmp);
        }
    }
    catch(e)
    {
        console.error(e);
    }

    console.log("전체 세션 복원 완료");
    // 전체 세션 복원
}
// 봇 작동시 함수 끗

client.on("invalidated", fautoReconnect);
function fautoReconnect(): void
{
    console.warn("client.invalidated");
    client.login(token);
}
// 자동 재연결 함수 끗

client.on("error", fclientError);
function fclientError(e: Error): void
{
    console.error("client.error");
    console.error(e.name);
    console.error(e.message);
}
// 에러 함수 끗

client.on("warn", fclientWarn);
function fclientWarn(e: string): void
{
    console.warn("client.warn");
    console.warn(e);
}
// 경고 함수 끗

client.on("messageCreate", fmessage);
function fmessage(msg: discord.Message): void
{
    if(msg.author.bot) return;
    // 쓸때없는 연산 금지

    console.log("event : messageCreate");
    client.user?.setActivity(__PREFIX + "정보");

    var nowIndex: number = -4444;

    if(sess.length != 0) for(var i: number = 0; i < sess.length; i++)
    {
        // console.log(sess.length + " " + i);
        // console.log(sess[i].guild().name + " " + msg.guild?.name);

        if(sess[i].guild().name === msg.guild?.name)
        {
            nowIndex = i;
            console.log("found session : " + sess[nowIndex].guild().name);

            break;
        }
    }
    // 세션 찾기

    if(nowIndex == -4444)
    {
        console.log("new session : " + sess[sess.push(new sessions.csessions(msg.guild as discord.Guild)) - 1].guild().name);
        if(msg.guild) list.push({guild: msg.guild.id, ttsroom: undefined});
        // 세션 추가

        for(var i: number = 0; i < sess.length; i++)
        {
            if(sess[i].guild().name === msg.guild?.name)
            {
                nowIndex = i;

                fs.writeFile("./backup.doctorchoithereader", JSON.stringify(list), {encoding: "utf-8"}, function(e: NodeJS.ErrnoException | null): void
                {
                    if(e) console.error(e);
                    else console.log("전체 세션 백업 완료");   
                });
                // @todo 전체 세션 백업
    
                break;
            }
        }
        // 이후 세션을 한번 새로 찾는다
    }

    if(msg.content.startsWith("?!??")) return;
    // TTS로 안읽게 하기 접두

    if(msg.content.startsWith(__PREFIX))
    {
        var command: string = msg.content.slice(__PREFIX.length);
        var args: Array<string> = command.split(' ');
        // 변수선언 끗

        args[0] = args[0].toLocaleLowerCase();
        // 코드 편의를 위해서

        if(args.length == 1) switch(args[0])
        // 명령어 처리 시작
        {
            case "정보": 
            case " info": 
                console.log(msg.author.tag + " : " + msg.content);

                msg.channel.send(new discord.MessagePayload(msg.channel, 
                    {
                        embeds: [
                        {
                            author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                            color: 0x0080C0, 
                            description: "https://gitlab.com/D0C70R_CH01/doctor_choi_the_reader  \nM4D3 I3Y 최박사 | D0C70R_CH01  \nTWITTER : @d0c70r_ch01  \nDISCORD : d0c70r_ch01  \n업타임 : " + fmakeTs() + "\n이 봇을 사용중인 서버 : " + sess.length + "개 \n\n명령어는 git의 README.md 참조  \n버그문의는 git에 이슈 달거나 트위터, 디스코드 디엠  \n\nVERSION " + __VERSION, 
                            footer: {text: msg.content}, 
                            title: "봇 정보", 
                            thumbnail: {url: String(client.user?.avatarURL())}
                        }]
                    })).catch(console.error);
                return;
////////////////////////////////////////////////////////////////////////////////////////////////
                //
                // 정보
                //
////////////////////////////////////////////////////////////////////////////////////////////////
            case "핑":
            case "ping":
                console.log(msg.author.tag + " : " + msg.content);

                msg.channel.send(new discord.MessagePayload(msg.channel, 
                {
                    embeds: [
                    {
                        author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                        color: 0x0080C0, 
                        description: client.ws.ping + "ms", 
                        footer: {text: msg.content}, 
                        title: "서버 딜레이", 
                        thumbnail: {url: String(client.user?.avatarURL())}
                    }]
                })).catch(console.error);
                return;
////////////////////////////////////////////////////////////////////////////////////////////////
                //
                // 핑
                //
////////////////////////////////////////////////////////////////////////////////////////////////
            case "설치":
            case "install":
                console.log(msg.author.tag + " : " + msg.content);

                try
                {
                    // ttsRoom = msg.channel;
                    // chatQueue = [];

                    if(msg.channel.type === discord.ChannelType.GuildText)
                    {
                        sess[nowIndex].ttsroom = msg.channel;
                        sess[nowIndex].chatQueue = [];

                        list[nowIndex].ttsroom = msg.channelId;
                    }
                }
                catch(e: any)
                {
                    msg.channel.send(new discord.MessagePayload(msg.channel, 
                    {
                        embeds: [
                        {
                            author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                            color: 0xC00000, 
                            description: "TTS봇 설치에 실패하였습니다.\n" + e.toString(), 
                            footer: {text: msg.content}, 
                            title: "ERROR (-2)", 
                            thumbnail: {url: String(client.user?.avatarURL())}
                        }]
                    })).catch(console.error);

                    console.warn(msg.author.tag + " : " + msg.content + " is ERROR -2")
                    return;
                }
                msg.channel.send(new discord.MessagePayload(msg.channel, 
                {
                    embeds: [
                    {
                        author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                        color: 0x0080C0, 
                        description: "명령어를 사용하여 음성 채널에 봇을 입장시키세요.", 
                        footer: {text: msg.content}, 
                        title: "TTS봇 설치 완료!", 
                        thumbnail: {url: String(client.user?.avatarURL())}
                    }]
                })).catch(console.error);

                fs.writeFile("./backup.doctorchoithereader", JSON.stringify(list), {encoding: "utf-8"}, function(e: NodeJS.ErrnoException | null): void
                {
                    if(e) console.error(e);
                    else console.log("전체 세션 백업 완료");   
                });
                // 전체 세션 백업

                return;
////////////////////////////////////////////////////////////////////////////////////////////////
                //
                // 설치
                //
////////////////////////////////////////////////////////////////////////////////////////////////
            case "제거":
            case "uninstall":
                console.log(msg.author.tag + " : " + msg.content);

                if(sess[nowIndex] == undefined)
                {
                    ferror1(msg);
                    return;
                }
                //설치가 안되어있으면 종료

                if(msg.channel.id != sess[nowIndex].ttsroom?.id) 
                    {
                        console.warn("debug : 1");
                        return;
                    }
                //설치된 채널이 아니면 종료

                try
                {
                    if(sess[nowIndex].joined == undefined) return;

                    sess[nowIndex].player.stop(true);
                    sess[nowIndex].joined?.destroy();
                    sess[nowIndex].joined = undefined;
                    sess[nowIndex].stop();
                }
                catch(e: any)
                {
                    msg.channel.send(new discord.MessagePayload(msg.channel, 
                    {
                        embeds: [
                        {
                            author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                            color: 0xC00000, 
                            description: "TTS봇 퇴장에 실패하였습니다.\n" + e.toString(), 
                            footer: {text: msg.content}, 
                            title: "ERROR (-5)", 
                            thumbnail: {url: String(client.user?.avatarURL())}
                        }]
                    })).catch(console.error);
                }
                // 음성방에 들어가 있으면 먼저 퇴장을 시도한다

                try
                {
                    sess[nowIndex].ttsroom = undefined;
                    list[nowIndex].ttsroom = undefined;
                }
                catch(e: any)
                {
                    msg.channel.send(new discord.MessagePayload(msg.channel, 
                    {
                        embeds: [
                        {
                            author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                            color: 0xC00000, 
                            description: "TTS봇 제거에 실패하였습니다.\n" + e.toString(), 
                            footer: {text: msg.content}, 
                            title: "ERROR (-3)", 
                            thumbnail: {url: String(client.user?.avatarURL())}
                        }]
                    })).catch(console.error);

                    console.warn(msg.author.tag + " : " + msg.content + " is ERROR -3")
                    return;
                }

                msg.channel.send(new discord.MessagePayload(msg.channel, 
                {
                    embeds: [
                    {
                        author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                        color: 0x0080C0, 
                        description: "더이상 봇이 이 채팅방을 읽지 않습니다.", 
                        footer: {text: msg.content}, 
                        title: "TTS봇 제거 완료!", 
                        thumbnail: {url: String(client.user?.avatarURL())}
                    }]
                })).catch(console.error);

                fs.writeFile("./backup.doctorchoithereader", JSON.stringify(list), {encoding: "utf-8"}, function(e: NodeJS.ErrnoException | null): void
                {
                    if(e) console.error(e);
                    else console.log("전체 세션 백업 완료");   
                });
                // 전체 세션 백업

                return;
////////////////////////////////////////////////////////////////////////////////////////////////
                //
                // 제거
                //
////////////////////////////////////////////////////////////////////////////////////////////////
            case "입장":
            case "join":
                console.log(msg.author.tag + " : " + msg.content);

                if(sess[nowIndex] == undefined)
                {
                    ferror1(msg);
                    return;
                }
                //설치가 안되어있으면 종료

                if(msg.channel != sess[nowIndex].ttsroom)
                {
                    ferror1(msg);
                    return;
                }
                //설치된 채널이 아니면 종료

                try
                {
                    if(msg.member?.voice.channelId == undefined || msg.member?.voice.channelId == null) throw new Error("음성 채팅에 먼저 입장해 주세요.");
                    if(msg.member?.voice.channelId == msg.guild?.afkChannel?.id) throw new Error("잠수 채널에는 참여할 수 없습니다.");
                    if(sess[nowIndex].joined != undefined) throw new Error("이미 음성 채널에 입장되어 있습니다.");

                    sess[nowIndex].joinedid = msg.member.voice.channelId;
                    sess[nowIndex].joined = discordVoice.joinVoiceChannel({channelId: msg.member?.voice.channelId as string, guildId: msg.guild?.id as string, adapterCreator: msg.guild?.voiceAdapterCreator as discordVoice.DiscordGatewayAdapterCreator, selfDeaf: false, selfMute: false});
                    // sess[nowIndex].joined?.subscribe(sess[nowIndex].player);
                    // // 이거 왜 undefined로 인식하냐?

                    sess[nowIndex].chatQueue = [];
                    sess[nowIndex].run();
                }
                catch(e: any)
                {
                    msg.channel.send(new discord.MessagePayload(msg.channel, 
                    {
                        embeds: [
                        {
                            author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                            color: 0xC00000, 
                            description: "TTS봇 입장에 실패하였습니다.\n" + e.toString(), 
                            footer: {text: msg.content}, 
                            title: "ERROR (-4)", 
                            thumbnail: {url: String(client.user?.avatarURL())}
                        }]
                    })).catch(console.error);

                    console.warn(msg.author.tag + " : " + msg.content + " is ERROR -4")
                    return;
                }
                msg.channel.send(new discord.MessagePayload(msg.channel, 
                {
                    embeds: [
                    {
                        author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                        color: 0x0080C0, 
                        description: "채팅을 치면 봇이 채팅을 읽어 줍니다.", 
                        footer: {text: msg.content}, 
                        title: "TTS봇 입장", 
                        thumbnail: {url: String(client.user?.avatarURL())}
                    }]
                })).catch(console.error);
                return;
////////////////////////////////////////////////////////////////////////////////////////////////
                //
                // 입장
                //
////////////////////////////////////////////////////////////////////////////////////////////////
            case "퇴장":
            case "leave":
                
                if(sess[nowIndex] == undefined)
                {
                    ferror1(msg);
                    return;
                }
                //설치가 안되어있으면 종료

                console.log(msg.author.tag + " : " + msg.content);

                if(sess[nowIndex].ttsroom == undefined)
                {
                    ferror1(msg);
                    return;
                }
                //설치가 안되어있으면 종료

                if(msg.channel != sess[nowIndex].ttsroom) return;
                //설치된 채널이 아니면 종료

                try
                {
                    if(sess[nowIndex].joined == undefined) return;

                    sess[nowIndex].player.stop(true);
                    sess[nowIndex].joined?.destroy();
                    sess[nowIndex].joined = undefined;
                    sess[nowIndex].stop();
                }
                catch(e: any)
                {
                    msg.channel.send(new discord.MessagePayload(msg.channel, 
                    {
                        embeds: [
                        {
                            author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                            color: 0xC00000, 
                            description: "TTS봇 퇴장에 실패하였습니다.\n" + e.toString(), 
                            footer: {text: msg.content}, 
                            title: "ERROR (-5)", 
                            thumbnail: {url: String(client.user?.avatarURL())}
                        }]
                    })).catch(console.error);

                    console.warn(msg.author.tag + " : " + msg.content + " is ERROR -5")
                    return;
                }
                msg.channel.send(new discord.MessagePayload(msg.channel, 
                {
                    embeds: [
                    {
                        author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                        color: 0x0080C0, 
                        description: "더이상 봇이 채팅을 읽어주지 않습니다.", 
                        footer: {text: msg.content}, 
                        title: "TTS봇 퇴장", 
                        thumbnail: {url: String(client.user?.avatarURL())}
                    }]
                })).catch(console.error);
                sess[nowIndex].last = "";
                sess[nowIndex].chatQueue = [];
                return;
////////////////////////////////////////////////////////////////////////////////////////////////
                //
                // 퇴장
                //
////////////////////////////////////////////////////////////////////////////////////////////////
            case "스킵":
            case "skip":
                console.log(msg.author.tag + " : " + msg.content);

                if(sess[nowIndex] == undefined)
                {
                    ferror1(msg);
                    return;
                }
                //설치가 안되어있으면 종료

                if(msg.channel != sess[nowIndex].ttsroom) return;
                //설치된 채널이 아니면 종료

                if(sess[nowIndex].joined == undefined) return;
                //음성방에 봇이 없으면 종료

                try
                {
                    sess[nowIndex].player.stop();
                }
                catch(e: any)
                {
                    msg.channel.send(new discord.MessagePayload(msg.channel, 
                        {
                            embeds: [
                            {
                                author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                                color: 0xC00000, 
                                description: "스킵에 실패하였습니다.\n" + e.toString(), 
                                footer: {text: msg.content}, 
                                title: "ERROR (-6)", 
                                thumbnail: {url: String(client.user?.avatarURL())}
                            }]
                        })).catch(console.error);
    
                        console.warn(msg.author.tag + " : " + msg.content + " is ERROR -6")
                        return;
                }
                msg.channel.send(new discord.MessagePayload(msg.channel, 
                    {
                        embeds: [
                        {
                            author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                            color: 0x0080C0, 
                            description: "쉿!", 
                            footer: {text: msg.content}, 
                            title: "스킵", 
                            thumbnail: {url: String(client.user?.avatarURL())}
                        }]
                    })).catch(console.error);
                return;
////////////////////////////////////////////////////////////////////////////////////////////////
                //
                // 스킵
                //
////////////////////////////////////////////////////////////////////////////////////////////////
            case "커피":
            case "coffee":
                    msg.channel.send(new discord.MessagePayload(msg.channel, 
                        {
                            embeds: [
                            {
                                author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())},
                                color: 0x0080C0,
                                description: "가난한 최박사에게 커피를 사주세요.\nhttps://www.buymeacoffee.com/d0c70rch01", 
                                footer: {text: msg.content}, 
                                title: "커피",
                                thumbnail: {url: String(client.user?.avatarURL())},
                                url: "https://www.buymeacoffee.com/d0c70rch01"
                        }]
                        })).catch(console.error);
                return;
////////////////////////////////////////////////////////////////////////////////////////////////
                //
                // 커피
                //
////////////////////////////////////////////////////////////////////////////////////////////////
        }
        //명령어 처리
    }
    // 명령어 접두로 시작할 시

    if(sess[nowIndex] == undefined) return;
    // 세션이 없으면 바로 리턴

    if(msg.channel == sess[nowIndex].ttsroom)
    {
        if(sess[nowIndex].ttsroom == undefined || sess[nowIndex].joined == undefined) return;
        if((msg.member?.voice.channelId != undefined || msg.member?.voice.channelId != null) && sess[nowIndex].ttsroom != undefined && msg.member.voice.channelId == sess[nowIndex].joinedid)
        {
            // chatQueue.push({name: msg.member.displayName, chat: msg.cleanContent});
            sess[nowIndex].add(new sessions.chats(msg.member.displayName, msg.cleanContent));

            console.log(msg.author.tag + " " + msg.cleanContent + " is added");
        }
        // 아닌 경우
    }

    // console.log(chatQueue);


    // console.log(sess[0]);
    // fs.writeFile("doctorchoi_the_reader.sessionbackup", JSON.stringify(sess), function(err : NodeJS.ErrnoException | null) { if(err) console.error(err); } );
    // 세션 백업
}
//메세지 처리 함수 끗