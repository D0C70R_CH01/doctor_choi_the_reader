/* CODE I3Y D0C70R_CH01 (https://gitlab.com/D0C70R_CH01) */

import * as textToSpeech from "@google-cloud/text-to-speech";
import fs from "fs";
// import getAudioDurationInSeconds from "get-audio-duration";
// import 끗

const __TEXT: string = "최박사 | D0C70R_CH01 said | 테스트용문구입니다람쥐썬더";

var client: textToSpeech.TextToSpeechClient = new textToSpeech.TextToSpeechClient();

var req: textToSpeech.protos.google.cloud.texttospeech.v1.ISynthesizeSpeechRequest | undefined = 
{
    input: {text: __TEXT},
    voice: {languageCode: "ko_KR", ssmlGender: "FEMALE", name: "ko-KR-Wavenet-A"},
    audioConfig: {audioEncoding: "MP3"}
};

client.synthesizeSpeech(req, ftts);
function ftts(e: any, res: any): void
{
    if(e)
    {
        console.error(e);
        return;
    }

    fs.writeFileSync("./tts/buffer.mp3", res.audioContent);

    // getAudioDurationInSeconds("./tts/buffer.mp3").then((dulation: number) => { console.log(dulation) });
}