/* CODE I3Y D0C70R_CH01 (https://gitlab.com/D0C70R_CH01) */

import discord from "discord.js";
import * as discordVoice from "@discordjs/voice";
import * as textToSpeech from "@google-cloud/text-to-speech";
import fs from "fs";
// import 끗

export class chats extends Object
{
    public name!: string;
    public chat!: string;

    constructor(_name: string, _chat: string)
    {
        super();

        this.name = _name;
        this.chat = _chat
    }
}
// chats 선언 끗

export class csessions extends Object
{
    private __SAID: string = "  said | ";
    private __VOICE: textToSpeech.protos.google.cloud.texttospeech.v1.IVoiceSelectionParams = {languageCode: "ko_KR", ssmlGender: "FEMALE", name: "ko-KR-Wavenet-A"};
    private __GUILD: discord.Guild | undefined = undefined;
    private __TTS: textToSpeech.TextToSpeechClient = new textToSpeech.TextToSpeechClient();
    // private 선언 끗

    public chatQueue: Array<chats> = [];
    public ttsroom: discord.TextChannel | undefined;
    public joined: discordVoice.VoiceConnection | undefined = undefined;
    public joinedid: string = String(Math.random() * 10000000);
    public last: string = "";
    public player: discordVoice.AudioPlayer = discordVoice.createAudioPlayer({behaviors: {noSubscriber: discordVoice.NoSubscriberBehavior.Play}});
    public timeout: number | NodeJS.Timeout | undefined = undefined;
    public timeout2: number | NodeJS.Timeout | undefined = undefined;
    // public 선언 끗

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

    private _fread(): void
    {
        this.timeout = setTimeout(() => { this._fread() }, 20);
        // this.timeout2 = setTimeout(() => { this._fname() }, 1000 * 60 * 5);
        // 일단 타임아웃 재설정

        if(this.player.state.status == discordVoice.AudioPlayerStatus.Playing) return;
        if(this.joined == null) return;
        if(this.joinedid == undefined) return;
        if(this.chatQueue.length == 0) return;
        // 쓸때없는 연산 금지

        var content: string;
        // 변수선언 끗

        if(this.last == this.chatQueue[0].name)
    {
        content = this.chatQueue[0].chat;
    }
    else
    {
        if(this.chatQueue[0].chat.match(/^[?!'.,]+$/))
        {
            this.chatQueue[0].chat = this.chatQueue[0].chat.replace(/[?]/g, "물음표 ");
            this.chatQueue[0].chat = this.chatQueue[0].chat.replace(/[!]/g, "느낌표 ");
            this.chatQueue[0].chat = this.chatQueue[0].chat.replace(/[']/g, "아포스트로피 ");
            this.chatQueue[0].chat = this.chatQueue[0].chat.replace(/[.]/g, "점 ");
            this.chatQueue[0].chat = this.chatQueue[0].chat.replace(/[,]/g, "쉼표 ");
        }
        // 안읽는거 읽게하기

        content = this.chatQueue[0].name + this.__SAID + this.chatQueue[0].chat;
    }

    this.last = this.chatQueue[0].name;

    content = content.replace(/(http|https|ftp|telnet|news|mms):\/\/[^\"'\s()]+/gi, "링크");
    // 링크 주소 치환

    var req: textToSpeech.protos.google.cloud.texttospeech.v1.ISynthesizeSpeechRequest | undefined = 
    {
        input: {text: content},
        voice: this.__VOICE,
        audioConfig: {audioEncoding: "MP3"}
    };

    try
    {
        this.__TTS.synthesizeSpeech(req, (e: any, res: any) =>
        {
            if(e)
            {
                console.error(e);
                this.timeout = setTimeout(() => { this._fread() }, 20);
                // 타임아웃 재설정
    
                return;
            }
            // 에러같은거 있으면 리턴
    
            this.timeout = setTimeout(() => { this._fread() }, 1100);
            // 채팅 읽는 시간을 위하여 타임아웃을 길게 잡음
    
            try
            {
                fs.writeFileSync("./tts/" + this.joinedid + "_buffer.mp3", res.audioContent);
            }
            catch(e: any)
            {
                console.error(e);
                this.timeout = setTimeout(() => { this._fread() }, 20);
                // 타임아웃 재설정
    
                return;
                // 에러생기면 리턴
                // 임시 코드
            }

            this.player.play(discordVoice.createAudioResource("./tts/" + this.joinedid + "_buffer.mp3")); // 이거 왜 에러
            this.chatQueue.shift();
            // tts 재생
    
            console.log("readed!");
        });
            // 텍스트 읽는 함수 끗   
    }
    catch(e: any)
    {
        console.error("TTS 생성 실패");
        this.timeout = setTimeout(() => { this._fread() }, 20);
        return;
    }
    // tts 생성

    clearTimeout(this.timeout);
    // 잠시 타임아웃 제거

    console.log(content + " is maked");
    // 디버그용
    }

    private  _fname(): void
    {
        this.last = "";
    }

 ////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

    constructor(guild: discord.Guild | undefined)
    {
        super();

        this.__GUILD = guild;
    }
    // 생성자 끗

    public guild(): discord.Guild
    {
        return this.__GUILD as discord.Guild;
    }
    // 길드 투척하는 함수 끗

    public add(chat: chats): void
    {
        this.chatQueue.push(chat);
    }
    // 추가 함수 끗

    public run(): void
    {
        if(this.joined != undefined) this.joined.subscribe(this.player);

        this._fread();
    }
    // 설치 완료후 작동 함수 끗

    public stop(): void
    {
        clearTimeout(this.timeout);
    }
    // 설치 완료후 작동하던거 멈추는 함수 끗
}
// 세션 관련 클래스 선언 끗